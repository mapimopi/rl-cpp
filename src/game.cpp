#include "main.h"

namespace game
{

static void newGame()
{
    ctx->currentState = GameState::Gameplay;
    systems::newGame();
}

static void init()
{
    ctx = new Context;
    raiseIfNull(ctx, "Game context allocation error");
    newGame();
}

static void quit()
{
    delete ctx;
}

static void update()
{
    switch (ctx->currentState) {
    case GameState::Gameplay: {
        gameplay::update();
    } break;
    case GameState::MainMenu: {
        mainmenu::update();
    } break;
    }
}

static void draw()
{
    switch (ctx->currentState) {
    case GameState::Gameplay: {
        gameplay::draw();
    } break;
    case GameState::MainMenu: {
        mainmenu::draw();
    } break;
    }
}

static void mainLoop()
{
    while (ctx->running) {
        timer::beforeStep();

        events::update(&ctx->event, update);

        if (ctx->draw) {
            renderer::beforeDraw();
            draw();
            renderer::afterDraw();
            ctx->draw = false;
        }

        timer::afterStep();
    }
}

} // namespace game
