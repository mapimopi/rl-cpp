constexpr int FPS = 30;
constexpr int TPS = 1000 / FPS;

struct TimerContext {
    unsigned int frameStart;
    unsigned int frameEnd;
    unsigned int ticks;
};

namespace timer
{

TimerContext *tctx;

static void init()
{
    tctx = new TimerContext;
    raiseIfNull(tctx, "Timer context allocation error");
}

static void quit()
{
    delete tctx;
}

static void beforeStep()
{
    tctx->frameStart = SDL_GetTicks();
}

static void afterStep()
{
    tctx->frameEnd = SDL_GetTicks();
    tctx->ticks = tctx->frameEnd - tctx->frameStart;

    if (tctx->ticks < TPS) {
        SDL_Delay((TPS) - (tctx->ticks));
    }

    tctx->frameStart = tctx->frameEnd;
}

static bool blink(int ms)
{
    return (SDL_GetTicks() / ms) % 2 > 0;
}

} // namespace timer
