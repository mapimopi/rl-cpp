struct RenderContext {
    GPU_Target *screen;
    SDL_Window *window;
    GPU_Image *tilesImage;
    GPU_Rect *src;
    FC_Font *font;

    int pixelWidth;
    int pixelHeight;
    int scaledWidth;
    int scaledHeight;

    int scale;

    SDL_Color fontColor;
};

namespace renderer
{

constexpr cstring TILE_PATH = "./assets/tiles16.png";
constexpr int TILE_WIDTH = 16;
constexpr int TILE_HEIGHT = 16;

constexpr cstring FONT_PATH = "./assets/RobotoSlab.ttf";
constexpr int FONT_SIZE = 18;

constexpr int SCALE = 1;

constexpr SDL_Color backgroundColor = {30, 25, 20, 255};

RenderContext *rctx;

static void resize(int w, int h)
{
    GPU_SetWindowResolution(w, h);

    int glWidth, glHeight;
    int sdlWidth, sdlHeight;
    SDL_GL_GetDrawableSize(rctx->window, &glWidth, &glHeight);
    SDL_GetWindowSize(rctx->window, &sdlWidth, &sdlHeight);

    rctx->scale = SCALE * glWidth / sdlWidth;
    rctx->pixelWidth = glWidth;
    rctx->pixelHeight = glHeight;
    rctx->scaledWidth = sdlWidth;
    rctx->scaledHeight = sdlHeight;
}

static void beforeDraw()
{
    GPU_ClearColor(rctx->screen, backgroundColor);
}

static void afterDraw()
{
    GPU_Flip(rctx->screen);
}

static void init(
    int initialWidth,
    int initialHeight,
    int minWidth = 100,
    int minHeight = 100)
{
    rctx = new RenderContext;
    raiseIfNull(rctx, "Render context allocation error");

    int flags = GPU_DEFAULT_INIT_FLAGS | SDL_WINDOW_SHOWN |
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI;

    rctx->screen = GPU_Init(initialWidth, initialHeight, flags);
    rctx->window = SDL_GetWindowFromID(rctx->screen->context->windowID);

    SDL_SetWindowMinimumSize(rctx->window, minWidth, minHeight);

    resize(initialWidth, initialHeight);

    rctx->tilesImage = GPU_LoadImage(TILE_PATH);
    GPU_SetImageFilter(rctx->tilesImage, GPU_FILTER_NEAREST);

    rctx->font = FC_CreateFont();
    FC_LoadFont(
        rctx->font,
        FONT_PATH,
        FONT_SIZE * rctx->scale,
        FC_MakeColor(255, 255, 255, 255),
        TTF_STYLE_NORMAL);

    rctx->src = new GPU_Rect;
}

static void quit()
{
    FC_FreeFont(rctx->font);
    delete rctx->src;
    GPU_FreeImage(rctx->tilesImage);
    GPU_Quit();
    delete rctx;
}


static int getPixelWidth()
{
    return rctx->pixelWidth;
}

static int getPixelHeight()
{
    return rctx->pixelHeight;
}

static int getScaledWidth()
{
    return rctx->scaledWidth;
}

static int getScaledHeight()
{
    return rctx->scaledHeight;
}

static int getTileWidth()
{
    return TILE_WIDTH;
}

static int getTileHeight()
{
    return TILE_HEIGHT;
}

static int getScale()
{
    return rctx->scale;
}

static int getScale(int value)
{
    return value * rctx->scale;
}

static int getLineHeight()
{
    return FC_GetLineHeight(rctx->font);
}


static void setClip(Rect rect)
{
    GPU_SetClip(rctx->screen, rect.x, rect.y, rect.w, rect.h);
}

static void unsetClip()
{
    GPU_UnsetClip(rctx->screen);
}

/* TILES  */

static void blit(
    GPU_Image *img,
    int srcX,
    int srcY,
    int dstX,
    int dstY,
    int width,
    int height)
{
    // TODO: Check if dstX and dstY are inside the screen region
    rctx->src->x = srcX * width;
    rctx->src->y = srcY * height;
    rctx->src->w = width;
    rctx->src->h = height;

    GPU_BlitScale(
        img,
        rctx->src,
        rctx->screen,
        dstX * rctx->scale + width * rctx->scale / 2,
        dstY * rctx->scale + height * rctx->scale / 2,
        rctx->scale,
        rctx->scale);
}

static void setTileColor(int r, int g, int b)
{
    GPU_SetRGB(rctx->tilesImage, r, g, b);
}

static void drawTile(int srcX, int srcY, int dstX, int dstY)
{
    blit(rctx->tilesImage, srcX, srcY, dstX, dstY, TILE_WIDTH, TILE_HEIGHT);
}

/* TEXT */

static void setTextColor(int r, int g, int b, int a = 255)
{
    rctx->fontColor = FC_MakeColor(r, g, b, a);
}

static void drawText(string text, int dstX, int dstY, int lineWidth = 0)
{
    if (text.length() > 0) {
        FC_DrawColor(
            rctx->font,
            rctx->screen,
            dstX,
            dstY,
            rctx->fontColor,
            "%s",
            text.c_str());
    }
}

static int getColumnHeight(string text, int width)
{
    if (text.length() > 0) {
        return FC_GetColumnHeight(rctx->font, width, "%s", text.c_str());
    } else {
        return 0;
    }
}

static int drawColumn(string text, int dstX, int dstY, int width)
{
    if (text.length() > 0) {
        return FC_DrawColumnColor(
                   rctx->font,
                   rctx->screen,
                   dstX,
                   dstY,
                   width,
                   rctx->fontColor,
                   "%s",
                   text.c_str())
            .h;
    } else {
        return 0;
    }
}

static void
drawTextWithCaret(string text, int caretPosition, int dstX, int dstY)
{
    auto [textLeft, textRight] = splitAt(text, caretPosition);

    FC_DrawColor(
        rctx->font,
        rctx->screen,
        dstX,
        dstY,
        rctx->fontColor,
        "%s",
        textLeft.c_str());

    dstX += FC_GetWidth(rctx->font, "%s", textLeft.c_str());

    cstring caret = "|";
    int caretOffsetX = -4;
    int caretOffsetY = 0;
    FC_DrawColor(
        rctx->font,
        rctx->screen,
        dstX + caretOffsetX,
        dstY + caretOffsetY,
        rctx->fontColor,
        "%s",
        caret);

    FC_DrawColor(
        rctx->font,
        rctx->screen,
        dstX,
        dstY,
        rctx->fontColor,
        "%s",
        textRight.c_str());
}

} // namespace renderer
