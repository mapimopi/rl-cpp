enum struct EventType {
    None,
    KeyInput,
    TextInput,
    ScrollInput,
    ClearInput,
    SubmitInput,
    WindowResize,
    Quit
};

enum struct Key {
    None,
    Backspace,
    Delete,
    Up,
    Down,
    Left,
    Right,
};

struct KeyInputEvent {
    Key key;
};

struct TextInputEvent {
    string text;
};

struct ScrollInputEvent {
    int y;
};

struct WindowResizeEvent {
    int width;
    int height;
};

struct Event {
    EventType type;
    variant<KeyInputEvent, TextInputEvent, ScrollInputEvent, WindowResizeEvent>
        payload;
};

struct EventContext {
    SDL_Event event;
};

namespace events
{

EventContext *ectx;

static void init()
{
    ectx = new EventContext;
    raiseIfNull(ectx, "Event context allocation error");
}

static void quit()
{
    delete ectx;
}

static Key symToKey(int sym)
{
    switch (sym) {
    case SDLK_BACKSPACE:
        return Key::Backspace;
    case SDLK_DELETE:
        return Key::Delete;
    case SDLK_UP:
        return Key::Up;
    case SDLK_DOWN:
        return Key::Down;
    case SDLK_LEFT:
        return Key::Left;
    case SDLK_RIGHT:
        return Key::Right;
    default:
        return Key::None;
    }
}

static void update(Event *event, void(update)(void))
{
    while (SDL_PollEvent(&ectx->event) != 0) {
        event->type = EventType::None;

        switch (ectx->event.type) {
        case SDL_QUIT: {
            event->type = EventType::Quit;
        } break;

        case SDL_WINDOWEVENT: {
            switch (ectx->event.window.event) {
            case SDL_WINDOWEVENT_RESIZED: {
                event->type = EventType::WindowResize;
                event->payload = WindowResizeEvent{ectx->event.window.data1,
                                                   ectx->event.window.data2};
            } break;
            }
        } break;

        case SDL_KEYDOWN: {
            switch (ectx->event.key.keysym.sym) {
            case SDLK_BACKSPACE:
            case SDLK_DELETE:
            case SDLK_UP:
            case SDLK_DOWN:
            case SDLK_LEFT:
            case SDLK_RIGHT: {
                event->type = EventType::KeyInput;
                event->payload =
                    KeyInputEvent{symToKey(ectx->event.key.keysym.sym)};
            } break;

            case SDLK_ESCAPE: {
                event->type = EventType::ClearInput;
            } break;

            case SDLK_RETURN: {
                event->type = EventType::SubmitInput;
            } break;
            }
        } break;

        case SDL_TEXTINPUT: {
            // TODO: Check for special keydown inputs here
            event->type = EventType::TextInput;
            event->payload = TextInputEvent{ectx->event.text.text};
        } break;

        case SDL_MOUSEWHEEL: {
            event->type = EventType::ScrollInput;
            event->payload = ScrollInputEvent{ectx->event.wheel.y};
        } break;

        default:
            event->type = EventType::None;
        }

        update();
    }
}

static void enableTextInput()
{
    SDL_StartTextInput();
}

static void disableTextInput()
{
    SDL_StopTextInput();
}

} // namespace events
