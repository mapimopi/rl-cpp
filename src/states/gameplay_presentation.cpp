#include "../main.h"

namespace gameplay
{

constexpr int LOG_WIDTH = 650;
constexpr int PADDING = 16;

constexpr int MAP_WIDTH = 11;
constexpr int MAP_HEIGHT = 11;

static void drawTiles()
{
    // renderer::drawTile(
    //     13,
    //     1,
    //     renderer::getScaledWidth() / 2 - renderer::getTileWidth() / 2,
    //     60);

    // int center = renderer::getScaledWidth() / 2;
    // int halfMap = MAP_WIDTH / 2 * renderer::getTileWidth();
    // int tileWidth = renderer::getTileHeight();
    // int tileHeight = renderer::getTileHeight();
    // int halfTile = tileWidth / 2;

    // Rect rect;
    // rect.x = center - halfMap - halfTile;
    // rect.y = renderer::getLineHeight() * 3 / renderer::getScale();
    // rect.w = renderer::getTileWidth() * MAP_WIDTH;
    // rect.h = renderer::getTileHeight() * MAP_HEIGHT;

    // float n;
    // int x, y;

    // renderer::setTileColor(120, 180, 80);
    // for (int i = 0; i < MAP_WIDTH; ++i) {
    //     for (int j = 0; j < MAP_HEIGHT; ++j) {
    //         n = noiseAt(i / 10.0f, j / 10.0f);
    //         x = i * tileWidth;
    //         y = j * tileHeight;

    //         if (n < 0.2f) {
    //             renderer::drawTile(4, 0, rect.x + x, rect.y + y);
    //         } else if (n < 0.4f) {
    //             renderer::drawTile(5, 1, rect.x + x, rect.y + y);
    //         } else if (n < 0.6f) {
    //             renderer::drawTile(6, 2, rect.x + x, rect.y + y);
    //         } else if (n < 0.8f) {
    //             renderer::drawTile(3, 0, rect.x + x, rect.y + y);
    //         } else {
    //             renderer::drawTile(8, 4, rect.x + x, rect.y + y);
    //         }
    //     }
    // }
}

static void drawUI()
{
    renderer::setTextColor(220, 200, 180);
    // renderer::drawText(
    //     string("<- Tiles haha"),
    //     renderer::getPixelWidth() / 2,
    //     renderer::getPixelHeight() / 2);
    // renderer::drawText(string("AAAA Hello"), 0, 1);
    // renderer::setTextColor(140, 140, 220);
    // renderer::drawText(string("AAAA Hello"), 0, 3, 5);
    // renderer::drawText(string("Expelliarmus"), 20, 3, 4);
}

static int getLogTop()
{
    return 0;
    // return renderer::getLineHeight() * 2 * renderer::getScale() +
    //     renderer::TILE_HEIGHT * MAP_HEIGHT * renderer::getScale();
}

static int getLogWidth()
{
    return min(
        renderer::getScale(LOG_WIDTH), renderer::getPixelWidth() - PADDING * 2);
}

static int getTextHeight(string text)
{
    return renderer::getColumnHeight(text, getLogWidth()) +
        renderer::getLineHeight();
}

static int getTextHeight(string text, int width)
{
    return renderer::getColumnHeight(text, width) + renderer::getLineHeight();
}

static Rect getLogRect()
{
    int marginTop = getLogTop();
    int marginBottom = renderer::getLineHeight() + PADDING;

    Rect rect;
    rect.w = getLogWidth();
    rect.h = renderer::getPixelHeight() - marginTop - marginBottom;
    rect.x = renderer::getPixelWidth() / 2 - rect.w / 2;
    rect.y = marginTop;
    return rect;
}

static void clampScroll()
{
    int min = 0;
    int max = -state.maxLogScroll;
    if (state.logScroll < min) {
        state.logScroll = min;
    }
    if (max > 0 && state.logScroll > max) {
        state.logScroll = max;
    }
}

static void drawRoomName()
{
    int x = renderer::getPixelWidth() / 2 - getLogWidth() / 2;
    int y = PADDING;

    renderer::setTextColor(180, 200, 220);
    renderer::drawColumn(
        string("Hello, World!\nRoom 1234."), x, y, getLogWidth());
}

static void drawLog()
{
    Rect rect = getLogRect();

    int visibleTop = rect.y;
    int visibleBottom = rect.y + rect.h;

    string *text;
    int top, bottom;

    clampScroll();
    int y = rect.y + rect.h + state.logScroll;

    renderer::setClip(rect);
    renderer::setTextColor(220, 200, 180);
    for (int i = ctx->log.entries.size() - 1; i >= 0; --i) {
        text = &ctx->log.entries[i].text;
        top = y - getTextHeight(*text, rect.w);
        bottom = y;

        if (bottom < visibleTop) {
            break;
        }

        if (top < visibleBottom && bottom > visibleTop) {
            renderer::drawColumn(*text, rect.x, top, rect.w);
        }

        y = top;

        // FIXME: state.maxLogScroll should be used in clampScroll
        if (i == 0) {
            state.maxLogScroll = bottom - (rect.y + rect.h + state.logScroll);
        } else {
            state.maxLogScroll = 0;
        }
    }
    renderer::unsetClip();

    // TODO: Clamp based on just drawn log lines
}

static void drawInputText()
{
    int x = renderer::getPixelWidth() / 2 - getLogWidth() / 2;
    int y = renderer::getPixelHeight() - renderer::getLineHeight() - PADDING;

    renderer::setTextColor(220, 220, 220);
    renderer::drawTextWithCaret(
        ctx->input.text, ctx->input.caretPosition, x, y);

    // if (timer::blink(400)) {
    //     renderer::drawTextWithCaret(
    //         ctx->input.text, ctx->input.caretPosition, x, y);
    // } else {
    //     renderer::drawText(ctx->input.text, x, y);
    // }
}

static void draw()
{
    // drawTiles();
    // drawUI();
    // drawRoomName();
    drawLog();
    drawInputText();
}

} // namespace gameplay
