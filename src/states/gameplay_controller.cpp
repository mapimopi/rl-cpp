#include "../main.h"

namespace gameplay
{

constexpr int keyboardScrollSpeed = 100;
constexpr int mousewheelScrollSpeed = 20;

static void quit()
{
    ctx->running = false;
}

static void markDirty()
{
    ctx->draw = true;
}

static void windowResize()
{
    WindowResizeEvent payload = get<WindowResizeEvent>(ctx->event.payload);
    renderer::resize(payload.width, payload.height);
    markDirty();
}

static void keyInput()
{
    KeyInputEvent payload = get<KeyInputEvent>(ctx->event.payload);

    switch (payload.key) {
    case Key::Backspace: {
        input::removeBackward();
        markDirty();
    } break;

    case Key::Delete: {
        input::removeForward();
        markDirty();
    } break;

    case Key::Up: {
        state.logScroll += keyboardScrollSpeed;
        markDirty();
    } break;

    case Key::Down: {
        state.logScroll -= keyboardScrollSpeed;
        markDirty();
    } break;

    case Key::Left: {
        input::moveCaretLeft();
        markDirty();
    } break;

    case Key::Right: {
        input::moveCaretRight();
        markDirty();
    } break;

    default:; // noop
    }
}

static void textInput()
{
    TextInputEvent payload = get<TextInputEvent>(ctx->event.payload);
    input::insertText(payload.text);
    markDirty();
}

static void scrollInput()
{
    ScrollInputEvent payload = get<ScrollInputEvent>(ctx->event.payload);
    state.logScroll += payload.y * mousewheelScrollSpeed;
    // TODO: smooth scroll
    markDirty();
}

static void clearInput()
{
    input::clear();
    state.logScroll = 0;
    markDirty();
}

static void submitInput()
{
    actions::parse();
    clearInput();
    markDirty();
}

static void update()
{
    switch (ctx->event.type) {
        EVENT_CASE(EventType::Quit, quit);
        EVENT_CASE(EventType::WindowResize, windowResize);
        EVENT_CASE(EventType::KeyInput, keyInput);
        EVENT_CASE(EventType::TextInput, textInput);
        EVENT_CASE(EventType::ScrollInput, scrollInput);
        EVENT_CASE(EventType::ClearInput, clearInput);
        EVENT_CASE(EventType::SubmitInput, submitInput);
    default:
        break;
    }
}

} // namespace gameplay
