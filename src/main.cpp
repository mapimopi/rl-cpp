#include "main.h"

#include "content/input.cpp"
#include "content/log.cpp"
#include "content/components.cpp"
#include "content/entities.cpp"
#include "content/actions.cpp"
#include "systems/global.cpp"
#include "states/mainmenu_controller.cpp"
#include "states/mainmenu_presentation.cpp"
#include "states/gameplay_controller.cpp"
#include "states/gameplay_presentation.cpp"
#include "game.cpp"

int main()
{
    timer::init();
    events::init();
    renderer::init(800, 600, 300, 600);
    actions::init();
    game::init();
    game::mainLoop();
    game::quit();
    renderer::quit();
    events::quit();
    timer::quit();

    return 0;
}
