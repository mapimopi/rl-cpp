inline static tuple<string, string> splitAt(string str, int position)
{
    return {str.substr(0, position),
            str.substr(position, str.length() - position)};
}

inline static string trim(string str)
{
    size_t first = str.find_first_not_of(' ');
    if (first == string::npos) {
        return "";
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

static string lowercase(string str)
{
    for (int i = 0; i < str.length(); ++i) {
        str[i] = tolower(str[i]);
    }
    return str;
}

static vector<string> split(string str, const char delim = ' ')
{
    vector<string> result;
    string toAdd;

    int start = 0;
    for (int i = 0; i < str.length(); ++i) {
        if (str[i] == delim) {
            toAdd = str.substr(start, (i - start));
            if (toAdd != "" && toAdd != " ") {
                result.push_back(toAdd);
            }
            start = i + 1;
        }
    }
    result.push_back(str.substr(start, (str.length() - start)));

    return result;
}
