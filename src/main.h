#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_gpu.h>
#include <iostream>
#include <vector>
#include <variant>
#include <stdexcept>

#define FC_USE_SDL_GPU 1
#include "vendor/SDL_FontCache.cpp"

#define STB_PERLIN_IMPLEMENTATION 1
#include "vendor/stb_perlin.h"

#define internal namespace

#define raiseIfNull(value, error)                                              \
    if (!value) {                                                              \
        throw runtime_error(error);                                            \
    }

#define EVENT_CASE(t, f)                                                 \
    case t: {                                                                  \
        f();                                                                   \
    } break;

using namespace std;
using cstring = const char *;
using EntityId = unsigned int;
using ComponentMask = unsigned long long int;

constexpr EntityId ENTITY_POOL = 256;

#include "helpers/string.h"
#include "helpers/math.h"
#include "content/input.h"
#include "content/log.h"
#include "content/components.h"
#include "content/entities.h"
#include "content/actions.h"
#include "platform/timer.h"
#include "platform/renderer.h"
#include "platform/events.h"
#include "states/mainmenu.h"
#include "states/gameplay.h"

enum struct GameState {
    MainMenu,
    Gameplay,
};

struct Context {
    bool running = true;
    bool draw = true;
    GameState currentState;
    Event event;
    Input input;
    Log log;
    EntityPool entityPool;
    ComponentPool componentPool;
};

Context *ctx;
