enum struct Direction { North, East, South, West };

struct Input {
    string text;
    int caretPosition = 0;
};
