struct LogEntry {
    string text;
};

struct Log {
    vector<LogEntry> entries;
};
