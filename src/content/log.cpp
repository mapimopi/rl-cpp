#include "../main.h"

namespace log
{

static void add(string text)
{
    LogEntry entry;
    entry.text = text;
    ctx->log.entries.push_back(entry);
}

static void clear()
{
    ctx->log.entries.clear();
}

} // namespace log
