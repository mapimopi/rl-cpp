#include "../main.h"

namespace input
{

static void removeBackward()
{
    if (ctx->input.text.length() > 0 && ctx->input.caretPosition > 0) {
        auto [textLeft, textRight] =
            splitAt(ctx->input.text, ctx->input.caretPosition);
        textLeft = textLeft.substr(0, textLeft.length() - 1);
        ctx->input.text = textLeft + textRight;
        ctx->input.caretPosition--;
    }
}

static void removeForward()
{
    if (ctx->input.text.length() > 0 &&
        ctx->input.caretPosition < ctx->input.text.length()) {
        auto [textLeft, textRight] =
            splitAt(ctx->input.text, ctx->input.caretPosition);
        textRight = textRight.substr(1);
        ctx->input.text = textLeft + textRight;
    }
}

static void moveCaretLeft()
{
    if (ctx->input.caretPosition > 0) {
        ctx->input.caretPosition--;
    }
}

static void moveCaretRight()
{
    if (ctx->input.caretPosition < ctx->input.text.length()) {
        ctx->input.caretPosition++;
    }
}

static void insertText(string text)
{
    auto [textLeft, textRight] =
        splitAt(ctx->input.text, ctx->input.caretPosition);
    ctx->input.text = textLeft + text + textRight;
    ctx->input.caretPosition += text.length();
}

static void clear()
{
    ctx->input.text.clear();
    ctx->input.caretPosition = 0;
}

} // namespace input
