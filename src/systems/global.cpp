#include "../main.h"

namespace systems
{

static void newGame()
{
    {
        Entity *e = entity::create();

        Position *pos = component::position(e->id);
        pos->x = 123;
        pos->y = 321;
        entity::add(e, PositionComponent);

        Actor *act = component::actor(e->id);
        act->foo = "Test";
        entity::add(e, ActorComponent);
    }

    {
        Entity *e = entity::get(0);
        cout << "e.id: " << e->id << endl;

        Position *pos = component::position(e->id);
        cout << "pos.x: " << pos->x << " pos.y: " << pos->y << endl;

        Actor *act = component::actor(e->id);
        cout << "act.x: " << act->foo << endl;

        if (entity::has(e, PositionComponent & ActorComponent)) {
            cout << "E have Position and Actor" << endl;
        } else {
            cout << "E don't have Position and Actor" << endl;
        }
    }

    log::add("Hello, World! This is the first log entry. It must wrap (to test this functionality you see), so I'm purposefully dragging it on and on...");
    log::add("And this is the second log entry.");
}

} // namespace systems
