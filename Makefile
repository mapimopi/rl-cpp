default: build run

INPUT=src/main.cpp

OUTPUT=bin/out

LIBS=\
	-lstdc++   \
	-lSDL2     \
	-lSDL2_ttf \
	-lSDL2_gpu \
	-framework OpenGL

OPTS=\
	-std=c++17 \
	-g         \
	-o $(OUTPUT)

build: clean prepare copy_assets
	clang++ $(LIBS) $(OPTS) $(INPUT)

clean:
	rm -rf ./bin

prepare:
	mkdir ./bin

copy_assets:
	cp -r ./assets ./bin/

run:
	$(OUTPUT)
